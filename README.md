python 3.9

安装依赖包
```Shell
pip install -r requirements.txt
```

打包命令
```Shell
pyinstaller -F .\auto_plus.py
```

打包后如果运行需要把config.ini配置文件复制到 auto_plus.exe 的同级目录